import sys
import datetime

from app.db import db
from app.models.contest import Contest
from app.models.problem import Problem
from app.models.solution import Solution
from app.models.test import Test
from app.models.user import User


drop = [Problem, Solution, Test, Contest, User,]
create = [User, Contest, Problem, Test, Solution]

for model in drop:
    try:
        model.drop_table()
        print("Модель {} удалена".format(model))
    except Exception as ex:
        print("Ошибка удаления модели {}".format(model))
        print(ex)


print()
for model in create:
    model.create_table()
    print("Модель {} создана".format(model))

print()
print("Добавление пользователей")
krupenin, _ = User.get_or_create(email = 'krupeninka@gmail.com', defaults = { 'role': 'admin' })



print()
print("Добавление задач")
problem_1 = Problem.create(id = 1, name='Тест 1', text = 'Описание тестовой задачи 1')
problem_2 = Problem.create(id = 2, name='Тест 2', text = 'Описание тестовой задачи 2')
problem_3 = Problem.create(id = 3, name='Тест 3', text = 'Описание тестовой задачи 3')
problem_4 = Problem.create(id = 4, name='Тест 4', text = 'Описание тестовой задачи 4')
problem_5 = Problem.create(id = 5, name='Тест 5', text = 'Описание тестовой задачи 5')
problem_6 = Problem.create(id = 6, name='Тест 6', text = 'Описание тестовой задачи 6')
problem_7 = Problem.create(id = 7, name='Тест 7', text = 'Описание тестовой задачи 7')
problem_8 = Problem.create(id = 8, name='Тест 8', text = 'Описание тестовой задачи 8')
problem_9 = Problem.create(id = 9, name='Тест 9', text = 'Описание тестовой задачи 9')
problem_10 = Problem.create(id = 10, name='Тест 10', text = 'Описание тестовой задачи 10')


print()
print("Добавление соревнований")
contest_1 = Contest.create(name = "Первое публичное соревнование", status='public',
        problem_list = [[1,2,3,4,5], [3,4,5,6,7], [5,6,7,8,9], [1,7,8,9,10], [2,3,4,9,10]])

contest_2 = Contest.create(name = "Второе публичное соревнование", status='public',
        problem_list = [[1,2,3], [4,5,6], [7,8,9,10]])

contest_3 = Contest.create(name = "Первое приватное соревнование", status='private',
        problem_list = [[1,2], [3,4], [5,6,7], [8,9,10]])

contest_4 = Contest.create(name = "Первое закрытое соревнование", status='close',
        problem_list = [[1], [2], [3], [4], [5], [6], [7], [8], [9], [10]])

contest_5 = Contest.create(name = "Второе закрытое соревнование", status='close',
        problem_list = [[1], [2], [3], [4], [5], [6], [7], [8], [9], [10]])
