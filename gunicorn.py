# -*- coding: utf-8 -*-

# Gunicorn веб сервер, использует для запуска приложения. Для запуска можно использовать команду:
# gunicorn server:app -c gunicorn.py
# В этом файле находятся настройки веб сервера с мини описаниями. Для простоты управления сервером
# на разных компьтерах, с разными настройками истользуется конфиг файл config/gunicorn.yml, в котором
# хранятся параметры для запуска сервера, если файл отсутствует или в нем не указаны какие либо
# настройки, то в таком случае будут использоватся настройки по умолчанию. По умолчанию файл обавлен
# в gitignore, для того того чтобы каждый разработчик имел возможность использовать свой набор
# параметров.
#
# Для того чтобы изменить настройки по умолчанию, нужно обавить файл config/gunicorn.yml и добавить
# туда требуемые настройки. Для получения минимального набора настроек нужно копировать файл
# config/gunicorn.sample.yml

import os
import sys
import yaml

from multiprocessing import cpu_count

sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from app import root, enviroment

# Подгружаем настройки гуникорна, если они есть в config/gunicorn.yml
try:
    path   = os.path.join(root, 'config', 'gunicorn.yml')
    conf = yaml.load(open(path, 'r'))
    conf = conf[enviroment]
except IOError:
    print "WARNING: Файл config/gunicorn.yml не указано не используется, используются настройки по умолчанию"
except TypeError:
    print "WARNING: В config/gunicorn.yml не указано окружение %s, используются настройки по умолчанию" % enviroment
finally:
    conf = {}

# Server socket -
# Это интерфейс для обеспечения обмена данными между Nginx и приложением. Имеет следующие типы:
# 'HOST', 'HOST:PORT', 'unix:PATH', по умолчанию 'unix:tmp/gunicorn.sock' путь к файлу в tmp деректориии
# Sample Gunicorn configuration file.
# raise
bind = conf.get('bind', ('unix:' + os.path.join(root, 'tmp', 'gunicorn.sock' )))

# backlog -
# Параметр отвечает за размер очереди одновременно ожидающих подключений к сокету, то есть
# инициированных , но еще не принятых сервером (established), для Gunicorn может принимать значение
# в диопазоне от 64 до 2048, по умолчанию 1024, это вполнедостаточно для приложения.

backlog = conf.get('backlog', 1024)

# workers -
# Задает количество запускаемых порцессов. Важно знать, сколько памяти занимает один процесс. Это
# нужно, чтобы вы могли запустить нужное количество воркеров, не опасаясь перегрузить оперативную
# память. По умолчению для production сервера используется по два на количество ядер плюс один.

workers = conf.get('workers', (cpu_count() * 2 + 1))

# worker_class -
# Тип используемого воркера. По умолчанию используется sync, он работает с большинством 'нормальные'
# типы нагрузок для фреймфорка Flask. Подробнее ознакомится с другими типами воркеров можно в
# оригинальной документации http://docs.gunicorn.org/en/latest/design.

worker_class = conf.get('worker_class', 'sync')


#  worker_connections -
# Максимальное количество одновременных подключений. Используется только для async воркеров.

# worker_connections = conf.get('worker_connections', 1000)

#
#   timeout - If a worker does not notify the master process in this
#       number of seconds it is killed and a new worker is spawned
#       to replace it.
#
#       Generally set to thirty seconds. Only set this noticeably
#       higher if you're sure of the repercussions for sync workers.
#       For the non sync workers it just means that the worker
#       process is still communicating and is not tied to the length
#       of time required to handle a single request.
#
#   keepalive - The number of seconds to wait for the next request
#       on a Keep-Alive HTTP connection.
#
#       A positive integer. Generally set in the 1-5 seconds range.
#

# workers = 1
# worker_class = 'sync'
# worker_connections = 1000
# timeout = 30
# keepalive = 2

#
#   spew - Install a trace function that spews every line of Python
#       that is executed when running the server. This is the
#       nuclear option.
#
#       True or False
#

# spew = False

#
# Server mechanics
#
#   daemon - Detach the main Gunicorn process from the controlling
#       terminal with a standard fork/fork sequence.
#
#       True or False
#
#   pidfile - The path to a pid file to write
#
#       A path string or None to not write a pid file.
#
#   user - Switch worker processes to run as this user.
#
#       A valid user id (as an integer) or the name of a user that
#       can be retrieved with a call to pwd.getpwnam(value) or None
#       to not change the worker process user.
#
#   group - Switch worker process to run as this group.
#
#       A valid group id (as an integer) or the name of a user that
#       can be retrieved with a call to pwd.getgrnam(value) or None
#       to change the worker processes group.
#
#   umask - A mask for file permissions written by Gunicorn. Note that
#       this affects unix socket permissions.
#
#       A valid value for the os.umask(mode) call or a string
#       compatible with int(value, 0) (0 means Python guesses
#       the base, so values like "0", "0xFF", "0022" are valid
#       for decimal, hex, and octal representations)
#
#   tmp_upload_dir - A directory to store temporary request data when
#       requests are read. This will most likely be disappearing soon.
#
#       A path to a directory where the process owner can write. Or
#       None to signal that Python should choose one on its own.
#

# pidfile = os.path.join( root, 'tmp', 'gunicorn.pid' )
# umask = 0
# user = None
# group = None
# tmp_upload_dir = None

#
#   Logging
#
#   logfile - The path to a log file to write to.
#
#       A path string. "-" means log to stdout.
#
#   loglevel - The granularity of log output
#
#       A string of "debug", "info", "warning", "error", "critical"
#

# errorlog = os.path.join(root, 'logs', 'development.log')
# loglevel = 'ingo'
# accesslog = os.path.join(root, 'logs', 'development.log')

#
# Process naming
#
#   proc_name - A base to use with setproctitle to change the way
#       that Gunicorn processes are reported in the system process
#       table. This affects things like 'ps' and 'top'. If you're
#       going to be running more than one instance of Gunicorn you'll
#       probably want to set a name to tell them apart. This requires
#       that you install the setproctitle module.
#
#       A string or None to choose a default of something like 'gunicorn'.
#

# proc_name = None

#
# Server hooks
#
#   post_fork - Called just after a worker has been forked.
#
#       A callable that takes a server and worker instance
#       as arguments.
#
#   pre_fork - Called just prior to forking the worker subprocess.
#
#       A callable that accepts the same arguments as after_fork
#
#   pre_exec - Called just prior to forking off a secondary
#       master process during things like config reloading.
#
#       A callable that takes a server instance as the sole argument.
#

# reload = True


# def post_fork(server, worker):
#     server.log.info("Worker spawned (pid: %s)", worker.pid)

# def pre_fork(server, worker):
#     pass

# def pre_exec(server):
#     server.log.info("Forked child, re-executing.")

# def when_ready(server):
#     server.log.info("Server is ready. Spawning workers")

# def worker_int(worker):
#     worker.log.info("worker received INT or QUIT signal")

#     ## get traceback info
#     import threading, sys, traceback
#     id2name = dict([(th.ident, th.name) for th in threading.enumerate()])
#     code = []
#     for threadId, stack in sys._current_frames().items():
#         code.append("\n# Thread: %s(%d)" % (id2name.get(threadId,""),
#             threadId))
#         for filename, lineno, name, line in traceback.extract_stack(stack):
#             code.append('File: "%s", line %d, in %s' % (filename,
#                 lineno, name))
#             if line:
#                 code.append("  %s" % (line.strip()))
#     worker.log.debug("\n".join(code))

# def worker_abort(worker):
#     worker.log.info("worker received SIGABRT signal")
