from flask import render_template

from app.flask import app
from app.models.user import User


@app.route('/')
def main_index():
    return render_template('main/index.haml')
