from datetime import datetime
from functools import wraps

from flask import render_template, request, flash, redirect
from flask.ext.login import login_required
from flask_login import current_user

from app.flask import app
from app.models.contest import Contest
from app.models.problem import Problem


def check_role(f):
    """ Декоратор который подгружает тест, и проверяет доступ к нему """
    @wraps(f)
    def decorated_function(*args, **kwargs):

        if not current_user.role == 'admin':
            flash('Вам не доступно данный раздел', 'danger')
            return redirect('/')

        return f(*args, **kwargs)
    return decorated_function


@app.route('/contests')
@login_required
@check_role
def contests_index():
    contests = Contest.select().order_by(Contest.id.asc())
    return render_template('contests/index.haml', contests=contests,
            h_row_class = _class_by_status)


@app.route('/contests/new')
@login_required
@check_role
def contests_new(id):
    contest = Contest()
    return render_template('contests/form.haml', contest=contest)


@app.route('/contests', methods=['POST'])
@login_required
@check_role
def contests_create():
    contest = Contest()
    return render_template('contests/form.haml')


@app.route('/contests/<int:id>')
@login_required
@check_role
def contests_show(id):
    contest = Contest.get(Contest.id == id)
    contest.problem_list = enumerate(contest.problem_list) # Хак. Для итерации по списку во вьюхе
    return render_template('contests/show.haml', contest=contest,
            h_problems=_get_problems_by_ids)

@app.route('/contests/<int:id>/edit')
@login_required
@check_role
def contests_edit(id):
    contest = Contest.get(Contest.id == id)
    return render_template('contests/form.haml', contest=contest)


@app.route('/contests/<int:id>', methods=['POST'])
@login_required
@check_role
def contests_update(id):
    contest = Contest.get(Contest.id == id)
    return render_template('contests/show.haml')


@app.route('/contests/<int:id>/delete')
@login_required
@check_role
def contests_delete(id):
    contest = Contest.get(Contest.id == id)
    return render_template('contests/show.haml')


def _class_by_status(contest):
    """ Хелпер. Определение CSS класса по статусу соревнования """
    if contest.status == 'public':
        return 'success'
    if contest.status == 'private':
        return 'info'
    if contest.status == 'close':
        return 'danger'

def _get_problems_by_ids(ids):
    """ Хелпер. Поиск задач по списку id """
    return Problem.select().where(Problem.id << ids)