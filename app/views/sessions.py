# coding:utf-8
import jwt

from flask import request, render_template, redirect, url_for, flash
from flask_login import current_user, login_user, logout_user
from flask_mail import Message

from app.flask import app, login_manager, mailer
from app.models.user import User
from app.models.contest import Contest
from app.models.test import Test

@login_manager.user_loader
def load_user(id):
    try:
        return User.get(User.id == int(id))
    except User.DoesNotExist:
        return None


@app.route('/sessions/new')
def sessions_new():
    if current_user.is_authenticated:
        return redirect('/')
    return render_template('sessions/new.haml')


@app.route('/sessions', methods=['POST'])
def sessions_create():
    email = request.form.get('email')

    if email:
        message = _make_message(email)
        mailer.send(message)
        flash("Письмо успешно отправлено вам на почту", 'success')

    return redirect('/sessions/new')


@app.route('/sessions/delete')
def sessions_delete():
    logout_user()

    return redirect('/sessions/new')

@app.route('/sessions/<token>')
def sessions_token(token):
    params = _decode_token(token)
    user, _ = User.get_or_create(email = params['email'])
    login_user(user)

    if params.get('contest_id'):
        contest = Contest.get(Contest.id == params.get('contest_id'))
        test, _ = Test.get_or_create(user=user, contest=contest)
        return redirect('/tests/' + str(test.id))
    return redirect('/')


def _make_message(email):
    """ Формирование сообщения"""

    text = """
        Добрый день.

        Вас приветствует сервис Астрал Контест. Для авторизации перейдиде по ссылке:
        {url}

        C уважением,
        Астрал Контест.
    """

    html = """
        <html lang="ru" style="padding: 0px; margin: 0px;">
        <head></head>
        <p style="">
             Добрый день.
        </p>

        <p style="">
            Вас приветствует сервис Астрал Контест. Для авторизации перейдиде по ссылке:
            <a href="{url}" target="_blank">{server}</a>
        <p>

        <p style="">
            C уважением,
            </br>
            Астрал Контест.
        </p>

        </html>

    """

    keys = {
        'url': app.config['SERVER_HOST'] + '/sessions/' + _encode_token(email),
        'server': app.config['SERVER_HOST'],
    }

    message = Message(subject = "Астрал Контест", recipients=[email], body=text.format(**keys), html=html.format(**keys))
    return message

def _encode_token(email):
    """ Создание JWT токена """
    token = jwt.encode({'email': email}, app.config['JWT_SECRET'], algorithm=app.config['JWT_ALGORITHM'])
    token = token.decode('ascii')
    return token


def _decode_token(token):
    """ Создание JWT токена """
    return jwt.decode(token.encode('ascii'), app.config['JWT_SECRET'], algorithms=app.config['JWT_ALGORITHM'])

