
import jwt

from flask import render_template, request, flash, redirect
from flask.ext.login import login_required
from flask_mail import Message

from app.flask import app, mailer
from app.models.user import User
from app.models.contest import Contest

@app.route('/invites')
def invites_new():
    contests = Contest.select().order_by(Contest.id.asc())
    return render_template('invites/new.haml', contests=contests)


@app.route('/invites/create', methods=['POST'])
@login_required
def invites_create():
    email = request.form.get('email')
    contest_id = request.form.get('contest_id')

    if email:
        message = _make_message(email, contest_id)
        app.logger.error(mailer.send(message))
        flash("Письмо успешно отправлено на почту", 'success')

    return redirect('invites')


def _make_message(email, contest_id):
    """ Формирование сообщения"""
    contest = Contest.get(Contest.id == contest_id)

    text = """
        Добрый день.

        Вас приветствует сервис Астрал Контест. Вам доступно новое тестирование: {contest_name}. Для получения доступа перейдиде по ссылке:
        {url}

        C уважением,
        Астрал Контест.
    """

    html = """
        <html lang="ru" style="padding: 0px; margin: 0px;">
            <head></head>
            <body>
                <p style="">
                    Добрый день.
                </p>

                <p style="">
                    Вас приветствует сервис Астрал Контест. Вам доступно новое тестирование: {contest_name}. Для получения доступа перейдиде по ссылке:
                    <a href="{url}" target="_blank">{server}</a>
                <p>

                <p style="">
                    C уважением,
                    </br>
                    Астрал Контест.
                </p>

            </body>
        </html>
    """

    keys = {
        'url': app.config['SERVER_HOST'] + '/sessions/' + _encode_token(email, contest_id),
        'server': app.config['SERVER_HOST'],
        'contest_name': contest.name
    }

    message = Message(subject = "Астрал Контест", recipients=[email], body=text.format(**keys), html=html.format(**keys))
    return message

def _encode_token(email, contest_id):
    """ Создание JWT токена """
    token = jwt.encode({'email': email, 'contest_id': contest_id}, app.config['JWT_SECRET'], algorithm=app.config['JWT_ALGORITHM'])
    token = token.decode('ascii')
    return token

