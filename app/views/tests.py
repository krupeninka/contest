from datetime import datetime
from functools import wraps

from flask import render_template, request, flash, redirect
from flask.ext.login import login_required
from flask_login import current_user

from app.flask import app
from app.models.test import Test, LETTERS
from app.models.problem import Problem
from app.models.solution import Solution


def load_test(f):
    """ Декоратор который подгружает тест, и проверяет доступ к нему """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        test = Test.get(Test.id == kwargs['id'])

        if not test.user_id == current_user.id:
            flash('Вам не доступно данное тестирование', 'danger')
            return redirect('/')

        return f(*args, **kwargs, test=test)
    return decorated_function


@app.route('/tests/<int:id>')
@login_required
@load_test
def tests_show(id, test):
    if test.started_at:
        return redirect('/tests/'+ str(id) + '/problem/1')
    else:
        return render_template('tests/show.haml', test=test, problems=_build_problems_list(test))


@app.route('/tests/<int:id>/start')
@login_required
@load_test
def tests_start(id, test):
    test.start()
    return redirect('/tests/' + str(id) + '/problem/1')


@app.route('/tests/<int:id>/problem/<int:index>')
@login_required
@load_test
def tests_problem(id, index, test):
    problem_id = test.test_list[index - 1]
    problem = Problem.get(Problem.id == problem_id)
    return render_template('tests/problem.haml', test=test, problem=problem,
            problems=_build_problems_list(test), solution=_build_solutions_list)


@app.route('/tests/<int:id>/problem/<int:index>', methods=['POST'])
@login_required
@load_test
def tests_problem_post(id, index, test):
    problem_id = test.test_list[index - 1]
    problem = Problem.get(Problem.id == problem_id)

    Solution.create(user_id=current_user.id, test=test, problem=problem, text=request.form['solution'])

    return render_template('tests/problem.haml', test=test, problem=problem,
            problems=_build_problems_list(test))


def _build_problems_list(test):
    """ Формируем словарь с задачами для отображения на фронте """
    problems = []

    for index, id in enumerate(test.test_list):
        problem = Problem.get(Problem.id == id)
        problem_dict = {}
        problem_dict['id'] = id
        problem_dict['letter'] = LETTERS[index]
        problem_dict['name'] = problem.name
        problems.append(problem_dict)

    return problems


def _build_solutions_list(test, problem)