import os
import logging

from flask import Flask
from flask_login import LoginManager
from flask_mail import Mail
from werkzeug import ImmutableDict
from logging.handlers import RotatingFileHandler

from app import root, enviroment


class FlaskHaml(Flask):
    """Обертка на Flask которая позволяет работать с haml"""

    jinja_options = ImmutableDict(
        extensions=['jinja2.ext.autoescape', 'jinja2.ext.with_', 'hamlish_jinja.HamlishExtension'])

# Инициализация приложения
app = FlaskHaml(__name__, instance_relative_config=True)
app.secret_key = '4689863de94441ea86744be632d12dbb93bab07d07584ccfb82cf1ce59b94d49'
# Подключаем кокфиги
app.config.from_pyfile(os.path.join(root, 'config', 'application.py'))
# Подключаем почту
mailer = Mail(app)

# Логирование приложения
handler = RotatingFileHandler(os.path.join(root, 'logs', enviroment + '.log'))
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)


# Авторизация пользователей
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "sessions_new"
