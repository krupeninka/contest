import hashlib

from peewee import CharField, PrimaryKeyField, BooleanField
from flask_login import UserMixin
from playhouse.signals import pre_save

from app.models._base import Base


class User(Base, UserMixin):
    """ Пользователи системы """

    id = PrimaryKeyField()
    email = CharField()
    active = BooleanField(default='t')
    role = CharField(default='user')

    invite = CharField(null=True, unique=True)