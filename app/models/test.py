import random

from datetime import datetime

from peewee import CharField, PrimaryKeyField, BooleanField, ForeignKeyField, DateTimeField
from playhouse.postgres_ext import JSONField
from playhouse.signals import pre_save

from app.models._base import Base
from app.models.contest import Contest
from app.models.user import User

LETTERS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']


class Test(Base):
    """ Модель для Тестирования """

    id = PrimaryKeyField()
    user = ForeignKeyField(User, index=True, related_name='tests')
    contest = ForeignKeyField(Contest, index=True, related_name='tests')
    test_list = JSONField()

    created_at = DateTimeField(index=True)
    started_at = DateTimeField(index=True, null=True)
    finished_at = DateTimeField(index=True, null=True)


    def start(self):
        """ Начало тестирование """
        len_list = len(self.contest.problem_list)
        random_index = random.randrange(0, len_list)
        ramdom_list = self.contest.problem_list[random_index]

        self.test_list = ramdom_list
        self.started_at = datetime.now()
        self.save()


@pre_save(sender=Test)
def test_set_created_at(model_class, instance, created):
    """ Автоматически ставим время создания """
    if not instance.created_at:
        instance.created_at = datetime.now()


@pre_save(sender=Test)
def test_set_report(model_class, instance, created):
    """Автоматически формируем словарь options, для удобства обработки"""
    if instance.test_list is None:
        instance.test_list = {}
