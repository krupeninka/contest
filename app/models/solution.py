from datetime import datetime

from peewee import CharField, PrimaryKeyField, BooleanField, ForeignKeyField, DateTimeField, TextField
from playhouse.postgres_ext import JSONField
from playhouse.signals import pre_save

from app.models._base import Base
from app.models.user import User
from app.models.contest import Contest
from app.models.test import Test


STATUSES = {
    'pending': 'Ожидает',
    'testing': 'Тестируется',
    'success': 'Успешно',
    'error': 'Ошибка'
    }


class Solution(Base):
    """ Модель для Решения """

    id = PrimaryKeyField()
    user = ForeignKeyField(User, index=True, related_name='solutions')
    problem = ForeignKeyField(Contest, index=True, related_name='solutions')
    test = ForeignKeyField(Test, index=True, related_name='solutions')
    text = TextField()
    status = CharField()

    report = JSONField()

    created_at = DateTimeField(index=True)
    finished_at = DateTimeField(index=True, null=True)


@pre_save(sender=Solution)
def solution_set_created_at(model_class, instance, created):
    """ Автоматически ставим время сохранения """
    if instance.created_at is None:
        instance.created_at = datetime.now()


@pre_save(sender=Solution)
def solution_set_report(model_class, instance, created):
    """Автоматически формируем словарь options, для удобства обработки"""
    if instance.report is None:
        instance.report = {}


@pre_save(sender=Solution)
def solution_set_status(model_class, instance, created):
    """Автоматически формируем словарь options, для удобства обработки"""
    if instance.status is None:
        instance.status = 'pending'
