from peewee import CharField, PrimaryKeyField, TextField

from app.models._base import Base


class Problem(Base):
    """ Модель для Задачи """

    id = PrimaryKeyField()
    name = CharField()
    text = TextField()
