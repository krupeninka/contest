from peewee import CharField, PrimaryKeyField, IntegerField
from playhouse.postgres_ext import JSONField
from playhouse.signals import pre_save

from app.models._base import Base
from app.models.problem import Problem

STATUSES = {
    'public': "Публичное",
    'private': "Скрытое",
    'close': "Закрытый"
}

class Contest(Base):
    """ Модель для Соревнования """

    id = PrimaryKeyField()
    name = CharField()
    status = CharField(index=True)
    problem_list = JSONField()


    def raw_status(self):
        """ Расшифровка статуса """
        try:
            return STATUSES[self.status]
        except KeyError:
            return self.status


@pre_save(sender=Contest)
def contest_set_problem_list(model_class, instance, created):
    """Автоматически формируем словарь options, для удобства обработки"""
    if instance.problem_list is None:
        instance.problem_list = {}
