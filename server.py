from app.flask import app

# Импортируем вьюхи
from app.views.sessions import *
from app.views.pages import *
from app.views.main import *
from app.views.invites import *
from app.views.tests import *
from app.views.contests import *

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
