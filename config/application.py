#-*- coding: utf-8 -*-

import os
import yaml

# определяем корень приложения Flask
ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Окружение в которой работает приложение, production, development или test.
# Устанавливается при запуске приложения, через переменную окружения ENV, например export ENV='production'.
# По умолчаниию development.
ENVIROMENT = os.environ.get('ENV', 'development')

# Загружаем конфигурацию виртуального окружение
path_config = os.path.join(ROOT, 'config', ENVIROMENT + '.yml')
env_config = yaml.load(open(path_config, 'r'))

# Настройки для подключения базы данных
path_config = os.path.join(ROOT, 'config', 'database.yml')
yaml_config = yaml.load(open(path_config, 'r'))

DATABASE = yaml_config.get(ENVIROMENT).get('database')
HOST     = yaml_config.get(ENVIROMENT).get('host')
PORT     = yaml_config.get(ENVIROMENT).get('port', 5432)
USERNAME = yaml_config.get(ENVIROMENT).get('username')
PASSWORD = yaml_config.get(ENVIROMENT).get('password')



# Настройки подключения почтовой рассылки
path_config = os.path.join(ROOT, 'config', 'base.yml')
yaml_config = yaml.load(open(path_config, 'r'))

MAIL_SERVER         = env_config['mail'].get('server')
MAIL_PORT           = env_config['mail'].get('port')
MAIL_USERNAME       = env_config['mail'].get('sender')
MAIL_PASSWORD       = env_config['mail'].get('password')
MAIL_DEFAULT_SENDER = env_config['mail'].get('sender')


# Данные сервера
SERVER_HOST = env_config['server'].get('host')

# Настройки для JWT
JWT_SECRET = env_config['jwt'].get('secret')
JWT_ALGORITHM = env_config['jwt'].get('algorithm')